package lu.sfeir.codinggame.controller.domain;

import lombok.Data;

/**
 * Created by renaudchardin on 12/03/2017.
 */
@Data
public class Response<T> {
    private T result;
    private Boolean error;
    private String errorMessage;
    private Integer codeStatus;
}
