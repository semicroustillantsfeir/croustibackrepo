package lu.sfeir.codinggame.controller.request;

import lombok.Data;
import lu.sfeir.codinggame.trip.enu.TransportMode;

import java.util.Date;

/**
 * Created by renaudchardin on 12/03/2017.
 */
@Data
public class TripRequest {
    TransportMode mode;
    String startLocation;
    String finishLocation;
    Boolean alternatives;
    Date arrivaleTime;
    Date departureTime;
}
