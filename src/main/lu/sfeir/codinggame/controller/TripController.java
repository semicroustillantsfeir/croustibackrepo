package lu.sfeir.codinggame.controller;

import lombok.extern.log4j.Log4j;
import lu.sfeir.codinggame.constant.URL;
import lu.sfeir.codinggame.controller.domain.Response;
import lu.sfeir.codinggame.controller.request.TripRequest;
import lu.sfeir.codinggame.trip.domain.Trip;
import lu.sfeir.codinggame.trip.service.TripService;
import lu.sfeir.codinggame.utils.RequestWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;

/**
 * Created by renaudchardin on 12/03/2017.
 */
@Log4j
@CrossOrigin
@RestController
@RequestMapping("/api")
public class TripController {
    @Autowired
    private TripService tripService;

    @PostMapping(path = URL.TRIP)
    public Response<List<Trip>> calculateTrip(@RequestBody TripRequest tripRequest) {
        Response<List<Trip>> result = new Response<>();
        try {
            Trip trip = tripService.calculTrip(tripRequest.getMode(), tripRequest.getStartLocation(), tripRequest.getFinishLocation(),
                    tripRequest.getAlternatives(), tripRequest.getArrivaleTime(), tripRequest.getDepartureTime());
            result.setResult(Arrays.asList(trip));
            result.setError(false);
            result.setCodeStatus(0);
        } catch (Exception e) {
            log.error(e);
            result.setResult(null);
            result.setError(true);
            result.setCodeStatus(1);
            result.setErrorMessage(e.getMessage());
        }
        return result;
    }


    @PostMapping(path = URL.PARKING_TRIP)
    public Response<List<Trip>> calculateTrip(@PathVariable String parkingId, @RequestBody TripRequest tripRequest) {
        Response<List<Trip>> result = new Response<>();
        try {
            List<Trip> trips = tripService.calculTripWithParking(tripRequest.getMode(), tripRequest.getStartLocation(), tripRequest.getFinishLocation(),
                    tripRequest.getAlternatives(), tripRequest.getArrivaleTime(), tripRequest.getDepartureTime(), parkingId);
            result.setResult(trips);
            result.setError(false);
            result.setCodeStatus(0);
        } catch (Exception e) {
            log.error(e);
            result.setResult(null);
            result.setError(true);
            result.setCodeStatus(1);
            result.setErrorMessage(e.getMessage());
        }
        return result;
    }

    @PostMapping(path = URL.BEST_TRIP)
    public Response<List<Trip>> calculateBestTrip(@RequestBody lu.sfeir.codinggame.controller.request.TripRequest tripRequest) {
        return RequestWrapper.execute(() -> tripService.getBetterTrips(tripRequest.getStartLocation(),
                tripRequest.getFinishLocation(), tripRequest.getArrivaleTime(), tripRequest.getDepartureTime()));
    }
}
