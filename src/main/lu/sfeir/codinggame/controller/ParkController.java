package lu.sfeir.codinggame.controller;

import lombok.extern.log4j.Log4j;
import lu.sfeir.codinggame.controller.domain.Response;
import lu.sfeir.codinggame.forecast.dto.ParkForecastDto;
import lu.sfeir.codinggame.forecast.service.ForecastService;
import lu.sfeir.codinggame.park.domain.ParkState;
import lu.sfeir.codinggame.park.dto.ParkDto;
import lu.sfeir.codinggame.park.service.ParkStateService;
import lu.sfeir.codinggame.repository.ParkStateRepository;
import lu.sfeir.codinggame.utils.DataGenerator;
import lu.sfeir.codinggame.utils.RequestWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.List;

/**
 * Created by Bastien on 11/03/2017.
 */
@Log4j
@CrossOrigin
@RestController
@RequestMapping("/api")
public class ParkController {
    @Autowired
    private ParkStateService parkStateService;
    @Autowired
    private ForecastService forecastService;

    @Autowired
    private ParkStateRepository parkStateRepository;

    @GetMapping(path = "/parks")
    public Response<List<ParkDto>> getTflData() {
        return RequestWrapper.execute(() -> parkStateService.getAllParksDto());
    }

    @GetMapping(path = "/parkStates")
    public Response<List<ParkState>> getParkStates() {
        return RequestWrapper.execute(() -> parkStateService.getAllParkStates());
    }

    @GetMapping(path = "/forecast")
    public Response<ParkForecastDto> getForecast(@RequestParam("date") long timestamp) {
        LocalDateTime date = LocalDateTime.ofInstant(Instant.ofEpochSecond(timestamp), ZoneOffset.UTC);
        return RequestWrapper.execute(() -> forecastService.getForecastDto(date));
    }

    @GetMapping(path = "generateData")
    public void generateData() {
        ParkState sample = new ParkState(parkStateService.getAllParks());
        DataGenerator.generateData(sample).stream().forEach(parkState -> parkStateRepository.save(parkState));
    }
}
