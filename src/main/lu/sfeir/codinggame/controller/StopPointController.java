package lu.sfeir.codinggame.controller;

import lombok.extern.log4j.Log4j;
import lu.sfeir.codinggame.controller.domain.Response;
import lu.sfeir.codinggame.stoppoint.domain.Departure;
import lu.sfeir.codinggame.stoppoint.domain.StopPoint;
import lu.sfeir.codinggame.stoppoint.service.StopPointService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.websocket.server.PathParam;
import java.util.List;

/**
 * Created by renaudchardin on 12/03/2017.
 */
@Log4j
@CrossOrigin
@RestController
@RequestMapping("/api")
public class StopPointController {
    @Autowired
    private StopPointService stopPointService;

    @GetMapping(path = "/stoppoint")
    public Response<List<StopPoint>> getAllStopPoint() {
        Response<List<StopPoint>> result = new Response<>();
        try {
            List<StopPoint> points = stopPointService.getAllStopPoint();
            result.setResult(points);
            result.setError(false);
            result.setCodeStatus(0);
        } catch (Exception e) {
            log.error(e);
            result.setResult(null);
            result.setError(true);
            result.setCodeStatus(1);
            result.setErrorMessage(e.getMessage());
        }
        return result;
    }

    @GetMapping(path = "/stoppoint/{id}")
    public Response<StopPoint> getStopPoint(@PathVariable String id) {
        Response<StopPoint> result = new Response<>();
        try {
            StopPoint point = stopPointService.getStopPoint(id);
            result.setResult(point);
            result.setError(false);
            result.setCodeStatus(0);
        } catch (Exception e) {
            log.error(e);
            result.setResult(null);
            result.setError(true);
            result.setCodeStatus(1);
            result.setErrorMessage(e.getMessage());
        }
        return result;
    }

    @GetMapping(path = "/stoppoint/around/{lon}/{la}/{radius}")
    public Response<List<StopPoint>> getStopPointAround(@PathVariable String lon, @PathVariable String la, @PathVariable Integer radius) {
        Response<List<StopPoint>> result = new Response<>();
        try {
            List<StopPoint> points = stopPointService.getStopPointAround(lon, la, radius);
            result.setResult(points);
            result.setError(false);
            result.setCodeStatus(0);
        } catch (Exception e) {
            log.error(e);
            result.setResult(null);
            result.setError(true);
            result.setCodeStatus(1);
            result.setErrorMessage(e.getMessage());
        }
        return result;
    }


    @GetMapping(path = "/stoppoint/departure/{id}")
    public Response<List<Departure>> getNextDeparture(@PathVariable String id) {
        Response<List<Departure>> result = new Response<>();
        try {
            List<Departure> departures = stopPointService.getNextDeparture(id);
            result.setResult(departures);
            result.setError(false);
            result.setCodeStatus(0);
        } catch (Exception e) {
            log.error(e);
            result.setResult(null);
            result.setError(true);
            result.setCodeStatus(1);
            result.setErrorMessage(e.getMessage());
        }
        return result;
    }
}

