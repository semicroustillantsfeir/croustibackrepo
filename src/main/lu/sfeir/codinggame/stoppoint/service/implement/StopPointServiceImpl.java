package lu.sfeir.codinggame.stoppoint.service.implement;

import lu.sfeir.codinggame.stoppoint.domain.Departure;
import lu.sfeir.codinggame.stoppoint.domain.StopPoint;
import lu.sfeir.codinggame.stoppoint.process.StopPointProcess;
import lu.sfeir.codinggame.stoppoint.service.StopPointService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by renaudchardin on 12/03/2017.
 */
@Service
public class StopPointServiceImpl implements StopPointService {

    @Autowired
    StopPointProcess stopPointProcess;

    @Override
    public List<StopPoint> getAllStopPoint() {
        return stopPointProcess.getAllStopPoint();
    }

    @Override
    public StopPoint getStopPoint(String id) {
        return stopPointProcess.getStopPoint(id);
    }

    @Override
    public List<StopPoint> getStopPointAround(String lon, String la, Integer radius) {
        return stopPointProcess.getStopPointAround(lon,la,radius);
    }

    @Override
    public List<Departure> getNextDeparture(String id) {
        return stopPointProcess.getNextDeparture(id);
    }
}
