package lu.sfeir.codinggame.stoppoint.service;

import lu.sfeir.codinggame.stoppoint.domain.Departure;
import lu.sfeir.codinggame.stoppoint.domain.StopPoint;

import java.util.List;

/**
 * Created by renaudchardin on 12/03/2017.
 */
public interface StopPointService {

    List<StopPoint> getAllStopPoint();

    StopPoint getStopPoint(String id);

    List<StopPoint> getStopPointAround(String lon, String la, Integer radius);

    List<Departure> getNextDeparture(String id);
}
