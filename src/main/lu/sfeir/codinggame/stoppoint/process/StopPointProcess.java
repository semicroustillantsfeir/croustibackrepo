package lu.sfeir.codinggame.stoppoint.process;

import lu.sfeir.codinggame.constant.TflUrl;
import lu.sfeir.codinggame.stoppoint.domain.Departure;
import lu.sfeir.codinggame.stoppoint.domain.StopPoint;
import lu.sfeir.codinggame.stoppoint.domain.StopPoints;
import lu.sfeir.codinggame.utils.RestProcess;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;

/**
 * Created by renaudchardin on 12/03/2017.
 */
@Component
public class StopPointProcess extends RestProcess {

    public List<StopPoint> getAllStopPoint() {
        return restTemplate.getForObject(TflUrl.STOP_POINT, StopPoints.class).stopPoints;
    }

    public StopPoint getStopPoint(String id) {
        return restTemplate.getForObject(String.format("%s/%s", TflUrl.STOP_POINT, id), StopPoint.class);
    }

    public List<StopPoint> getStopPointAround(String lon, String la, Integer radius) {
        return restTemplate.getForObject(String.format("%s/around/%s/%s/%d", TflUrl.STOP_POINT, lon, la, radius), StopPoints.class).stopPoints;
    }

    public List<Departure> getNextDeparture(String id) {
        return Arrays.asList(restTemplate.getForObject(String.format("%s/Departures/%s", TflUrl.STOP_POINT, id),Departure[].class));
    }
}
