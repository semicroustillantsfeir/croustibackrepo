package lu.sfeir.codinggame.stoppoint.domain;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * Created by renaudchardin on 12/03/2017.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "type",
        "geometry",
        "properties"
})
public class StopPoint {

    @JsonProperty("type")
    public String type;
    @JsonProperty("geometry")
    public Geometry geometry;
    @JsonProperty("properties")
    public Properties properties;

}
