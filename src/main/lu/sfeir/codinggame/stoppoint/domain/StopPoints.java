
package lu.sfeir.codinggame.stoppoint.domain;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.List;


@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "type",
        "stopPoints"
})
public class StopPoints {

    @JsonProperty("type")
    public String type;
    @JsonProperty("features")
    public List<StopPoint> stopPoints = null;

}
