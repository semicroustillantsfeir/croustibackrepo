package lu.sfeir.codinggame.stoppoint.domain;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "id",
        "type",
        "trainId",
        "line",
        "number",
        "departure",
        "delay",
        "live",
        "departureISO",
        "destination",
        "destinationId"
})
public class Departure {

    @JsonProperty("id")
    public String id;
    @JsonProperty("type")
    public String type;
    @JsonProperty("trainId")
    public Object trainId;
    @JsonProperty("line")
    public String line;
    @JsonProperty("number")
    public Integer number;
    @JsonProperty("departure")
    public Integer departure;
    @JsonProperty("delay")
    public Integer delay;
    @JsonProperty("live")
    public Boolean live;
    @JsonProperty("departureISO")
    public String departureISO;
    @JsonProperty("destination")
    public String destination;
    @JsonProperty("destinationId")
    public Integer destinationId;

}