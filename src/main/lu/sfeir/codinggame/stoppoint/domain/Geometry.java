package lu.sfeir.codinggame.stoppoint.domain;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.List;

/**
 * Created by renaudchardin on 12/03/2017.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "type",
        "coordinates"
})
public class Geometry {

    @JsonProperty("type")
    public String type;
    @JsonProperty("coordinates")
    public List<Double> coordinates = null;

}
