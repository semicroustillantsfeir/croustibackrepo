package lu.sfeir.codinggame.park.domain;

import com.fasterxml.jackson.annotation.*;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by renaudchardin on 11/03/2017.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Meta {

    @JsonProperty("open")
    private Boolean open;
    @JsonProperty("elevator")
    private Boolean elevator;
    @JsonProperty("link")
    private String link;
    @JsonProperty("address")
    private Address address;
    @JsonProperty("phone")
    private Integer phone;
    @JsonProperty("reserved_for_disabled")
    private Integer reservedForDisabled;
    @JsonProperty("reserved_for_women")
    private Integer reservedForWomen;
    @JsonProperty("motorbike_lots")
    private Integer motorbikeLots;
    @JsonProperty("bus_lots")
    private Integer busLots;
    @JsonProperty("bicycle_docks")
    private Integer bicycleDocks;
    @JsonProperty("payment_methods")
    private PaymentMethods paymentMethods;
    @JsonProperty("restrictions")
    private Restrictions restrictions;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("open")
    public Boolean getOpen() {
        return open;
    }

    @JsonProperty("open")
    public void setOpen(Boolean open) {
        this.open = open;
    }

    @JsonProperty("elevator")
    public Boolean getElevator() {
        return elevator;
    }

    @JsonProperty("elevator")
    public void setElevator(Boolean elevator) {
        this.elevator = elevator;
    }

    @JsonProperty("link")
    public String getLink() {
        return link;
    }

    @JsonProperty("link")
    public void setLink(String link) {
        this.link = link;
    }

    @JsonProperty("address")
    public Address getAddress() {
        return address;
    }

    @JsonProperty("address")
    public void setAddress(Address address) {
        this.address = address;
    }

    @JsonProperty("phone")
    public Integer getPhone() {
        return phone;
    }

    @JsonProperty("phone")
    public void setPhone(Integer phone) {
        this.phone = phone;
    }

    @JsonProperty("reserved_for_disabled")
    public Integer getReservedForDisabled() {
        return reservedForDisabled;
    }

    @JsonProperty("reserved_for_disabled")
    public void setReservedForDisabled(Integer reservedForDisabled) {
        this.reservedForDisabled = reservedForDisabled;
    }

    @JsonProperty("reserved_for_women")
    public Integer getReservedForWomen() {
        return reservedForWomen;
    }

    @JsonProperty("reserved_for_women")
    public void setReservedForWomen(Integer reservedForWomen) {
        this.reservedForWomen = reservedForWomen;
    }

    @JsonProperty("motorbike_lots")
    public Integer getMotorbikeLots() {
        return motorbikeLots;
    }

    @JsonProperty("motorbike_lots")
    public void setMotorbikeLots(Integer motorbikeLots) {
        this.motorbikeLots = motorbikeLots;
    }

    @JsonProperty("bus_lots")
    public Integer getBusLots() {
        return busLots;
    }

    @JsonProperty("bus_lots")
    public void setBusLots(Integer busLots) {
        this.busLots = busLots;
    }

    @JsonProperty("bicycle_docks")
    public Integer getBicycleDocks() {
        return bicycleDocks;
    }

    @JsonProperty("bicycle_docks")
    public void setBicycleDocks(Integer bicycleDocks) {
        this.bicycleDocks = bicycleDocks;
    }

    @JsonProperty("payment_methods")
    public PaymentMethods getPaymentMethods() {
        return paymentMethods;
    }

    @JsonProperty("payment_methods")
    public void setPaymentMethods(PaymentMethods paymentMethods) {
        this.paymentMethods = paymentMethods;
    }

    @JsonProperty("restrictions")
    public Restrictions getRestrictions() {
        return restrictions;
    }

    @JsonProperty("restrictions")
    public void setRestrictions(Restrictions restrictions) {
        this.restrictions = restrictions;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public Meta clone() {
        Meta clone = new Meta();
        clone.setOpen(this.open.booleanValue());
        return clone;
    }
}
