package lu.sfeir.codinggame.park.domain;

/**
 * Created by renaudchardin on 11/03/2017.
 */
import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.*;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonPropertyOrder({
        "cash",
        "vpay",
        "visa",
        "mastercard",
        "eurocard",
        "amex",
        "call2park"
})
public class PaymentMethods {

    @JsonProperty("cash")
    private Boolean cash;
    @JsonProperty("vpay")
    private Boolean vpay;
    @JsonProperty("visa")
    private Boolean visa;
    @JsonProperty("mastercard")
    private Boolean mastercard;
    @JsonProperty("eurocard")
    private Boolean eurocard;
    @JsonProperty("amex")
    private Boolean amex;
    @JsonProperty("call2park")
    private Boolean call2park;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("cash")
    public Boolean getCash() {
        return cash;
    }

    @JsonProperty("cash")
    public void setCash(Boolean cash) {
        this.cash = cash;
    }

    @JsonProperty("vpay")
    public Boolean getVpay() {
        return vpay;
    }

    @JsonProperty("vpay")
    public void setVpay(Boolean vpay) {
        this.vpay = vpay;
    }

    @JsonProperty("visa")
    public Boolean getVisa() {
        return visa;
    }

    @JsonProperty("visa")
    public void setVisa(Boolean visa) {
        this.visa = visa;
    }

    @JsonProperty("mastercard")
    public Boolean getMastercard() {
        return mastercard;
    }

    @JsonProperty("mastercard")
    public void setMastercard(Boolean mastercard) {
        this.mastercard = mastercard;
    }

    @JsonProperty("eurocard")
    public Boolean getEurocard() {
        return eurocard;
    }

    @JsonProperty("eurocard")
    public void setEurocard(Boolean eurocard) {
        this.eurocard = eurocard;
    }

    @JsonProperty("amex")
    public Boolean getAmex() {
        return amex;
    }

    @JsonProperty("amex")
    public void setAmex(Boolean amex) {
        this.amex = amex;
    }

    @JsonProperty("call2park")
    public Boolean getCall2park() {
        return call2park;
    }

    @JsonProperty("call2park")
    public void setCall2park(Boolean call2park) {
        this.call2park = call2park;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
