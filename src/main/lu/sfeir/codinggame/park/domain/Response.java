package lu.sfeir.codinggame.park.domain;

/**
 * Created by renaudchardin on 11/03/2017.
 */

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Response {

    private String type;
    private List<Park> features = null;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<Park> getFeatures() {
        return features;
    }

    public void setFeatures(List<Park> features) {
        this.features = features;
    }
}