package lu.sfeir.codinggame.park.domain;

import lombok.*;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Bastien on 11/03/2017.
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Data
public class ParkState implements Cloneable {
    private List<Park> parks;
    private LocalDateTime date;

    public ParkState(List<Park> parks) {
        this.parks = parks;
        this.date = LocalDateTime.now();
    }

    public ParkState clone() {
        ParkState clone = new ParkState();
        clone.setDate(this.date);;
        clone.setParks(this.getParks().stream().map(park -> park.clone()).collect(Collectors.toList()));
        return clone;
    }
}
