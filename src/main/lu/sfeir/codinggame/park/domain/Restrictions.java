package lu.sfeir.codinggame.park.domain;

/**
 * Created by renaudchardin on 11/03/2017.
 */
import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.annotation.*;

@JsonIgnoreProperties(ignoreUnknown = true)
@JsonPropertyOrder({
        "allowed_gpl",
        "allowed_trailor",
        "allowed_truck",
        "max_height"
})
public class Restrictions {

    @JsonProperty("allowed_gpl")
    private Boolean allowedGpl;
    @JsonProperty("allowed_trailor")
    private Boolean allowedTrailor;
    @JsonProperty("allowed_truck")
    private Boolean allowedTruck;
    @JsonProperty("max_height")
    private Integer maxHeight;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("allowed_gpl")
    public Boolean getAllowedGpl() {
        return allowedGpl;
    }

    @JsonProperty("allowed_gpl")
    public void setAllowedGpl(Boolean allowedGpl) {
        this.allowedGpl = allowedGpl;
    }

    @JsonProperty("allowed_trailor")
    public Boolean getAllowedTrailor() {
        return allowedTrailor;
    }

    @JsonProperty("allowed_trailor")
    public void setAllowedTrailor(Boolean allowedTrailor) {
        this.allowedTrailor = allowedTrailor;
    }

    @JsonProperty("allowed_truck")
    public Boolean getAllowedTruck() {
        return allowedTruck;
    }

    @JsonProperty("allowed_truck")
    public void setAllowedTruck(Boolean allowedTruck) {
        this.allowedTruck = allowedTruck;
    }

    @JsonProperty("max_height")
    public Integer getMaxHeight() {
        return maxHeight;
    }

    @JsonProperty("max_height")
    public void setMaxHeight(Integer maxHeight) {
        this.maxHeight = maxHeight;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
