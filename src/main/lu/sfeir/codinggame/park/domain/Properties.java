package lu.sfeir.codinggame.park.domain;

/**
 * Created by renaudchardin on 11/03/2017.
 */

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Properties {

    private String id;
    private String name;
    private Integer total;
    private Integer free;
    private String trend;
    private Meta meta;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public Integer getFree() {
        return free;
    }

    public void setFree(Integer free) {
        this.free = free;
    }

    public String getTrend() {
        return trend;
    }

    public void setTrend(String trend) {
        this.trend = trend;
    }

    public Meta getMeta() {
        return meta;
    }

    public void setMeta(Meta meta) {
        this.meta = meta;
    }

    public Properties clone() {
        Properties clone = new Properties();
        clone.setFree(this.free == null ? null : this.free.intValue());
        clone.setId(this.id);
        clone.setMeta(this.meta.clone());
        clone.setName(this.name);
        clone.setTotal(this.total == null ? null : this.total.intValue());
        clone.setTrend(this.trend);
        return clone;
    }
}
