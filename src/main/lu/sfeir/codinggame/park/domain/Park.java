package lu.sfeir.codinggame.park.domain;
import com.fasterxml.jackson.annotation.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created by renaudchardin on 11/03/2017.
 */
@Getter
@Setter
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class Park {
    private String type;
    private Geometry geometry;
    private Properties properties;

    public Park clone() {
        Park clone = new Park();
        clone.setGeometry(this.geometry);
        clone.setType(this.type);
        clone.setProperties(this.properties.clone());
        return clone;
    }
}
