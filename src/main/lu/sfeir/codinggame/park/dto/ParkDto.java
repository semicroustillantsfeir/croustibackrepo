package lu.sfeir.codinggame.park.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lu.sfeir.codinggame.park.domain.Meta;
import lu.sfeir.codinggame.park.domain.Park;
import lu.sfeir.codinggame.park.domain.Properties;

/**
 * Created by Bastien on 11/03/2017.
 */
@Getter
@Setter
@NoArgsConstructor
public class ParkDto {
    private String id;
    private String name;
    private Integer availableSpaces;
    private Integer totalSpaces;
    private String trend;
    private Boolean open;
    private String address;

    public ParkDto(Park park) {
        Properties properties = park.getProperties();
        Meta meta = properties.getMeta();

        this.id = properties.getId();
        this.name = properties.getName();
        this.availableSpaces = properties.getFree();
        this.totalSpaces = properties.getTotal();
        this.trend = properties.getTrend();
        this.open = meta.getOpen();
        if(meta.getAddress() != null)
            this.address = meta.getAddress().getStreet();
    }
}
