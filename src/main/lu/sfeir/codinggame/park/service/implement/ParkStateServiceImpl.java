package lu.sfeir.codinggame.park.service.implement;

import lu.sfeir.codinggame.constant.DataParameters;
import lu.sfeir.codinggame.park.domain.Park;
import lu.sfeir.codinggame.park.domain.ParkState;
import lu.sfeir.codinggame.park.dto.ParkDto;
import lu.sfeir.codinggame.park.process.ParkStateProcess;
import lu.sfeir.codinggame.park.service.ParkStateService;
import lu.sfeir.codinggame.repository.ParkStateRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Created by renaudchardin on 11/03/2017.
 */
@Service
public class ParkStateServiceImpl implements ParkStateService {


    @Autowired
    private ParkStateProcess parkStateProcess;
    @Autowired
    private ParkStateRepository parkStateRepository;

    @Override
    @Scheduled(cron = "0 */" + DataParameters.DUMP_FREQUENCY + " * * * *")
    public void collectTflData() {
        List<Park> parks = parkStateProcess.getAllParks().stream().collect(Collectors.toList());
        ParkState parkState = new ParkState(parks);

        parkStateRepository.save(parkState);
    }

    @Override
    public Optional<ParkState> getParkState(LocalDateTime date) {
        LocalDateTime reference = date.withSecond(0);
        List<ParkState> result = parkStateRepository.findByDateBetween(reference.minusMinutes(DataParameters.DUMP_FREQUENCY/2), reference.plusMinutes(DataParameters.DUMP_FREQUENCY/2));

        return result.isEmpty() ? Optional.empty() : Optional.of(result.get(0));
    }

    @Override
    public List<ParkState> getAllParkStates() {
        List<ParkState> result = parkStateRepository.findAll().subList(0, 1);
        result.sort((o1, o2) -> o1.getDate().isBefore(o2.getDate()) ? 1 : -1);
        return result;
    }

    @Override
    public List<Park> getAllParks() {
        return parkStateProcess.getAllParks();
    }

    @Override
    public List<ParkDto> getAllParksDto() {
        return parkStateProcess.getAllParks()
                .stream()
                .map(ParkDto::new)
                .sorted(Comparator.comparing(ParkDto::getName))
                .collect(Collectors.toList());
    }
}
