package lu.sfeir.codinggame.park.service;

import lu.sfeir.codinggame.park.domain.Park;
import lu.sfeir.codinggame.park.domain.ParkState;
import lu.sfeir.codinggame.park.dto.ParkDto;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

/**
 * Created by renaudchardin on 11/03/2017.
 */
public interface ParkStateService {
    void collectTflData();
    Optional<ParkState> getParkState(LocalDateTime date);
    List<ParkState> getAllParkStates();
    List<Park> getAllParks();
    List<ParkDto> getAllParksDto();
}
