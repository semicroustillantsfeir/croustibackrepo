package lu.sfeir.codinggame.park.process;

import lu.sfeir.codinggame.constant.TflUrl;
import lu.sfeir.codinggame.park.domain.Park;
import lu.sfeir.codinggame.park.domain.Response;
import lu.sfeir.codinggame.utils.RestProcess;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.List;

/**
 * Created by renaudchardin on 11/03/2017.
 */
@Component
public class ParkStateProcess extends RestProcess {
    public List<Park> getAllParks() {
        Response parks = restTemplate.getForObject(TflUrl.PARK, Response.class);
        return parks.getFeatures();
    }

    public Park getPark(String id) {
        return restTemplate.getForObject(TflUrl.PARK + "/" + id, Park.class);
    }
}
