package lu.sfeir.codinggame.utils;

import lu.sfeir.codinggame.constant.DataParameters;
import lu.sfeir.codinggame.park.domain.ParkState;
import org.springframework.util.SerializationUtils;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.function.Function;

/**
 * Created by Bastien on 12/03/2017.
 */
public class DataGenerator {
    private static Random random = new Random();

    public static List<ParkState> generateData(ParkState sample) {
        List<ParkState> generatedData = new ArrayList<>();
        LocalDateTime now = LocalDateTime.now().withHour(0).withMinute(0).withSecond(0);

        int i = 60;
        for(; i >= 45; i--)
            generateOneDay(now.minusDays(i), getHalfFree(), sample).forEach((parkState -> generatedData.add(parkState)));
        for(; i >= 30; i--)
            generateOneDay(now.minusDays(i), getFull(), sample).forEach((parkState -> generatedData.add(parkState)));
        for(; i >= 15; i--)
            generateOneDay(now.minusDays(i), getAlmostEmpty(), sample).forEach((parkState -> generatedData.add(parkState)));
        for(; i >= 0; i--)
            generateOneDay(now.minusDays(i), getHalfFree(), sample).forEach((parkState -> generatedData.add(parkState)));

        return generatedData;
    }

    private static List<ParkState> generateOneDay(LocalDateTime day, Function<Integer, Integer> strategy, final ParkState sample) {
        final Integer dumpByDay = 24 * 60 / DataParameters.DUMP_FREQUENCY;
        List<ParkState> parkStates = new ArrayList<>();

        for(int i = 0; i < dumpByDay; i++) {
            LocalDateTime date = day.plusMinutes(i * DataParameters.DUMP_FREQUENCY);
            ParkState parkState = sample.clone();

            System.out.println(date);
            parkState.getParks().forEach(park -> {
                Integer free = park.getProperties().getFree() != null ? strategy.apply(park.getProperties().getTotal()) : null;
                park.getProperties().setFree(free);
            });
            parkState.setDate(date);
            parkStates.add(parkState);
        }

        return parkStates;
    }

    private static Function<Integer, Integer> getAlmostEmpty() { return total -> (int)Math.round(total * (10d + random.nextInt(20)) / 100); };
    private static Function<Integer, Integer> getHalfFree() { return total -> (int)Math.round(total * (40d + random.nextInt(20)) / 100); };
    private static Function<Integer, Integer> getFull() { return total -> (int)Math.round(total * (80d + random.nextInt(20)) / 100); };
}
