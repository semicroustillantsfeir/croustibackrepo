package lu.sfeir.codinggame.utils;

import lombok.extern.log4j.Log4j;
import lu.sfeir.codinggame.controller.domain.Response;

import java.util.function.Supplier;

/**
 * Created by Bastien on 12/03/2017.
 */
@Log4j
public class RequestWrapper {
    public static <T> Response execute(Supplier<T> supplier) {
        Response<T> response = new Response<>();

        try {
            T result = supplier.get();
            response.setResult(result);
            response.setError(false);
            response.setCodeStatus(0);
        } catch (Exception e) {
            log.error(e);
            response.setResult(null);
            response.setError(true);
            response.setCodeStatus(1);
            response.setErrorMessage(e.getMessage());
        }

        return response;
    }

}
