package lu.sfeir.codinggame.forecast.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lu.sfeir.codinggame.park.domain.Park;
import lu.sfeir.codinggame.park.domain.ParkState;

import java.time.LocalDateTime;
import java.util.List;

/**
 * Created by Bastien on 12/03/2017.
 * This class has the same attributes than ParkState, but the semantic is different.
 * All attributes values are expectations based on some kind of average and comparison.
 */
@Getter
@Setter
@AllArgsConstructor
public class ParkForecast extends ParkState {
    public ParkForecast(List<Park> parks, LocalDateTime date) {
        super(parks, date);
    }
}
