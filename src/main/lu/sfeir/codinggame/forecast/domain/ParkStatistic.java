package lu.sfeir.codinggame.forecast.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lu.sfeir.codinggame.park.domain.Park;
import lu.sfeir.codinggame.park.domain.Properties;

/**
 * Created by Bastien on 12/03/2017.
 */
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class ParkStatistic {
    private Integer availableSpaces;
    private Integer occurrenceCount;
    private Double deviation;
    private Park park;

    public ParkStatistic(Park park) {
        this.availableSpaces = park.getProperties().getFree();
        if (this.availableSpaces == null) {
            this.deviation = 0d;
            this.occurrenceCount = 0;
        } else {
            this.deviation = Math.sqrt(this.availableSpaces);
            this.occurrenceCount = 1;
        }
        this.park = park;
    }

    public void append(Park park) {
        Integer newAvailableSpaces = park.getProperties().getFree();

        if (newAvailableSpaces != null) {
            this.deviation += Math.sqrt(newAvailableSpaces);
            this.occurrenceCount++;
            this.availableSpaces += newAvailableSpaces;
        }
    }

    public Park toPark() {
        Properties properties = this.park.getProperties();

        if (this.availableSpaces == null)
            properties.setFree(null);
        else
            properties.setFree(this.availableSpaces / this.occurrenceCount);

        this.park.setProperties(properties);

        return this.park;
    }
}
