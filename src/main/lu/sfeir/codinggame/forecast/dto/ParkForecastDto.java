package lu.sfeir.codinggame.forecast.dto;

import lombok.*;
import lu.sfeir.codinggame.park.domain.Park;
import lu.sfeir.codinggame.park.domain.ParkState;
import lu.sfeir.codinggame.park.dto.ParkDto;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Bastien on 12/03/2017.
 * This class has the same attributes than ParkState, but the semantic is different.
 * All attributes values are expectations based on some kind of average and comparison.
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Data
public class ParkForecastDto {
    private List<ParkDto> parks;
    private LocalDateTime date;
}
