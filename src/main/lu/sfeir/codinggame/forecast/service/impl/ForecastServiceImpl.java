package lu.sfeir.codinggame.forecast.service.impl;

import lu.sfeir.codinggame.constant.DataParameters;
import lu.sfeir.codinggame.forecast.domain.ParkForecast;
import lu.sfeir.codinggame.forecast.domain.ParkStatistic;
import lu.sfeir.codinggame.forecast.dto.ParkForecastDto;
import lu.sfeir.codinggame.forecast.service.ForecastService;
import lu.sfeir.codinggame.park.domain.Park;
import lu.sfeir.codinggame.park.domain.ParkState;
import lu.sfeir.codinggame.park.dto.ParkDto;
import lu.sfeir.codinggame.park.service.ParkStateService;
import org.apache.tomcat.jni.Local;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Created by Bastien on 11/03/2017.
 */
@Component
public class ForecastServiceImpl implements ForecastService {

    @Autowired
    private ParkStateService parkStateService;

    @Override
    public ParkForecast getForecast(LocalDateTime date) {
        return previousSameDay(date);
    }

    @Override
    public ParkForecastDto getForecastDto(LocalDateTime date) {
        ParkForecast result = getForecast(date);
        List<ParkDto> parks = result.getParks().stream().map((ParkDto::new)).collect(Collectors.toList());
        return new ParkForecastDto(parks, result.getDate());
    }

    //If date is a "monday", then return the ParkState's of the previous mondays
    private ParkForecast previousSameDay(LocalDateTime referenceDate) {
        LocalDateTime now = LocalDateTime.now(); //Optimisation

        while(referenceDate.isAfter(now))
            referenceDate = referenceDate.minusWeeks(1);

        List<ParkState> parkStates = IntStream.rangeClosed(1, DataParameters.NUMBER_OF_WEEK_CONSIDERED)
                .boxed()
                .parallel()
                .map(referenceDate::minusWeeks)
                .map((date) -> parkStateService.getParkState(date).orElse(null))
                .collect(Collectors.toList());

        parkStates.removeIf(Objects::isNull);

        ParkForecast forecast = new ParkForecast();
        HashMap<String, ParkStatistic> statistics = new HashMap<>();

        parkStates.forEach(parkState ->
            parkState.getParks().forEach(park -> processStatistic(statistics, park))
        );

        List<Park> parks = statistics
                .values()
                .stream()
                .map(ParkStatistic::toPark)
                .collect(Collectors.toList());
        ParkForecast aggregation = new ParkForecast(parks, referenceDate);

        return aggregation;
    }

    //Manage a park situation in the current statistics
    private void processStatistic(HashMap<String, ParkStatistic> statistics, Park park) {
        String parkId = park.getProperties().getId();
        ParkStatistic statistic = statistics.get(parkId);
        if (statistic == null)
            statistics.put(parkId, new ParkStatistic(park));
        else
            statistic.append(park);
    }

    //Check it by using the standard deviation
    private Boolean isSpecificPeriod() {
        return true;
    }
}
