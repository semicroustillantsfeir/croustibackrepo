package lu.sfeir.codinggame.forecast.service;

import lu.sfeir.codinggame.forecast.domain.ParkForecast;
import lu.sfeir.codinggame.forecast.dto.ParkForecastDto;
import lu.sfeir.codinggame.park.domain.ParkState;

import java.time.LocalDateTime;

/**
 * Created by Bastien on 11/03/2017.
 */
public interface ForecastService {
    ParkForecast getForecast(LocalDateTime date);
    ParkForecastDto getForecastDto(LocalDateTime date);
}
