package lu.sfeir.codinggame.trip.domain;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * Created by renaudchardin on 11/03/2017.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "icon",
        "name",
        "type"
})
public class Vehicle {

    @JsonProperty("icon")
    public String icon;
    @JsonProperty("name")
    public String name;
    @JsonProperty("type")
    public String type;

}
