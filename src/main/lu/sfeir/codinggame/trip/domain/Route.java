package lu.sfeir.codinggame.trip.domain;

/**
 * Created by renaudchardin on 11/03/2017.
 */

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "bounds",
        "legs",
        "summary",
        "warnings"
})
public class Route {

    @JsonProperty("bounds")
    public Bounds bounds;
    @JsonProperty("legs")
    public List<Leg> legs = null;
    @JsonProperty("summary")
    public String summary;
    @JsonProperty("warnings")
    public List<String> warnings = null;

}
