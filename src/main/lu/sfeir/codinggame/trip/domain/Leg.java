package lu.sfeir.codinggame.trip.domain;

/**
 * Created by renaudchardin on 11/03/2017.
 */

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "arrival_time",
        "departure_time",
        "distance",
        "duration",
        "end_address",
        "end_location",
        "start_address",
        "start_location",
        "steps"
})
public class Leg {

    @JsonProperty("arrival_time")
    public ArrivalTime arrivalTime;
    @JsonProperty("departure_time")
    public DepartureTime departureTime;
    @JsonProperty("distance")
    public Distance distance;
    @JsonProperty("duration")
    public Duration duration;
    @JsonProperty("end_address")
    public String endAddress;
    @JsonProperty("end_location")
    public EndLocation endLocation;
    @JsonProperty("start_address")
    public String startAddress;
    @JsonProperty("start_location")
    public StartLocation startLocation;
    @JsonProperty("steps")
    public List<Step> steps = null;

}
