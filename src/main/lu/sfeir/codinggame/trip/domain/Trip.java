package lu.sfeir.codinggame.trip.domain;

/**
 * Created by renaudchardin on 11/03/2017.
 */

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.List;


@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "routes",
        "status"
})
public class Trip {

    @JsonProperty("routes")
    public List<Route> routes = null;
    @JsonProperty("status")
    public String status;

}
