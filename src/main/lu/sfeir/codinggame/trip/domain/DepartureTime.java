package lu.sfeir.codinggame.trip.domain;

/**
 * Created by renaudchardin on 11/03/2017.
 */

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "text",
        "time_zone",
        "value"
})
public class DepartureTime {

    @JsonProperty("text")
    public String text;
    @JsonProperty("time_zone")
    public String timeZone;
    @JsonProperty("value")
    public Integer value;

}
