package lu.sfeir.codinggame.trip.domain;

/**
 * Created by renaudchardin on 11/03/2017.
 */

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "distance",
        "duration",
        "end_location",
        "html_instructions",
        "polyline",
        "start_location",
        "steps",
        "travel_mode",
        "transit_details"
})
public class Step {

    @JsonProperty("distance")
    public Distance_ distance;
    @JsonProperty("duration")
    public Duration_ duration;
    @JsonProperty("end_location")
    public EndLocation_ endLocation;
    @JsonProperty("html_instructions")
    public String htmlInstructions;
    @JsonProperty("polyline")
    public Polyline polyline;
    @JsonProperty("start_location")
    public StartLocation_ startLocation;
    @JsonProperty("steps")
    public List<Step_> steps = null;
    @JsonProperty("travel_mode")
    public String travelMode;
    @JsonProperty("transit_details")
    public TransitDetails transitDetails;

}
