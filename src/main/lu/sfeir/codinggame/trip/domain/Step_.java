package lu.sfeir.codinggame.trip.domain;

/**
 * Created by renaudchardin on 11/03/2017.
 */

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "distance",
        "duration",
        "end_location",
        "html_instructions",
        "start_location",
        "travel_mode"
})
public class Step_ {

    @JsonProperty("distance")
    public Distance__ distance;
    @JsonProperty("duration")
    public Duration__ duration;
    @JsonProperty("end_location")
    public EndLocation__ endLocation;
    @JsonProperty("html_instructions")
    public String htmlInstructions;
    @JsonProperty("start_location")
    public StartLocation__ startLocation;
    @JsonProperty("travel_mode")
    public String travelMode;
}
