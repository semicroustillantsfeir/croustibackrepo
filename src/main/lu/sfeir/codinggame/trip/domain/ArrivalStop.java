package lu.sfeir.codinggame.trip.domain;

/**
 * Created by renaudchardin on 11/03/2017.
 */

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "location",
        "name"
})
public class ArrivalStop {

    @JsonProperty("location")
    public Location location;
    @JsonProperty("name")
    public String name;

}
