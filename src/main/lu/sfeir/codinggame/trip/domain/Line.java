package lu.sfeir.codinggame.trip.domain;

/**
 * Created by renaudchardin on 11/03/2017.
 */

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "agencies",
        "name",
        "short_name",
        "vehicle"
})
public class Line {

    @JsonProperty("agencies")
    public List<Agency> agencies = null;
    @JsonProperty("name")
    public String name;
    @JsonProperty("short_name")
    public String shortName;
    @JsonProperty("vehicle")
    public Vehicle vehicle;

}
