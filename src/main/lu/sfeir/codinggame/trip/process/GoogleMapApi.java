package lu.sfeir.codinggame.trip.process;

import lu.sfeir.codinggame.constant.GoogleMap;
import lu.sfeir.codinggame.trip.domain.Trip;
import lu.sfeir.codinggame.trip.enu.TransportMode;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.Date;

/**
 * Created by renaudchardin on 11/03/2017.
 */
@Component
public class GoogleMapApi {

    public Trip calculTrip(TransportMode mode, String startLocation, String finishLocation,Boolean  alternatives, Date arrivaleTime,Date departureTime) throws RuntimeException {
        if(arrivaleTime !=null & departureTime !=null) throw new RuntimeException("Can't set departure and arrival time");

        RestTemplate restTemplate = new RestTemplate();
        restTemplate.getMessageConverters().add(new MappingJackson2HttpMessageConverter());
        StringBuilder builder = new StringBuilder(GoogleMap.DIRECTION);
        builder.append(GoogleMap.ORIGIN_PARAM).append(GoogleMap.EQUALS).append(startLocation);
        builder.append(GoogleMap.AND).append(GoogleMap.DESTINATION_PARAM).append(GoogleMap.EQUALS).append(finishLocation);
        builder.append(GoogleMap.AND).append(GoogleMap.ALTERNATIVES_PARAM).append(GoogleMap.EQUALS).append(alternatives.toString());
        if(arrivaleTime != null)
            builder.append(GoogleMap.AND).append(GoogleMap.ARRIVAL_TIME_PARAM).append(GoogleMap.EQUALS).append(arrivaleTime.getTime());
        if (departureTime != null && (TransportMode.DRIVING.equals(mode) || TransportMode.TRANSIT.equals(mode)))
            builder.append(GoogleMap.AND).append(GoogleMap.DEPARTURE_TIM_PARAM).append(GoogleMap.EQUALS).append(departureTime.getTime());
        builder.append(GoogleMap.AND).append(GoogleMap.MODE_PARAM).append(GoogleMap.EQUALS).append(mode);
        builder.append(GoogleMap.KEY_PARAM);

        return restTemplate.getForObject(builder.toString(), Trip.class);
    }

}
