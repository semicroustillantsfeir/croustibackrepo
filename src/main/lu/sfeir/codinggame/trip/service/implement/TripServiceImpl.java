package lu.sfeir.codinggame.trip.service.implement;

import lu.sfeir.codinggame.constant.GoogleMap;
import lu.sfeir.codinggame.forecast.service.ForecastService;
import lu.sfeir.codinggame.park.domain.Park;
import lu.sfeir.codinggame.park.process.ParkStateProcess;
import lu.sfeir.codinggame.park.service.ParkStateService;
import lu.sfeir.codinggame.trip.domain.Trip;
import lu.sfeir.codinggame.trip.enu.TransportMode;
import lu.sfeir.codinggame.trip.process.GoogleMapApi;
import lu.sfeir.codinggame.trip.service.TripService;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by renaudchardin on 12/03/2017.
 */
@Service
public class TripServiceImpl implements TripService {

    @Autowired
    GoogleMapApi googleMapApi;

    @Autowired
    ParkStateProcess parkState;

    @Autowired
    private ForecastService forecastService;

    @Autowired
    private ParkStateService parkStateService;

    @Override
    public Trip calculTrip(TransportMode mode, String startLocation, String finishLocation, Boolean alternatives, Date arrivaleTime, Date departureTime) throws RuntimeException {
        return googleMapApi.calculTrip(mode, startLocation, finishLocation, alternatives, arrivaleTime, departureTime);
    }

    @Override
    public List<Trip> calculTripWithParking(TransportMode mode, String startLocation, String finishLocation, Boolean alternatives, Date arrivaleTime, Date departureTime, String parkingId) throws RuntimeException {
        List<Trip> trips = new ArrayList<>();
        Park park = parkState.getPark(parkingId);
        if (park != null) {
            String parkingCoordonate = String.format("%s,%s", park.getGeometry().getCoordinates().get(1).toString(), park.getGeometry().getCoordinates().get(0).toString());
            if (arrivaleTime != null) {
                Trip trip = googleMapApi.calculTrip(TransportMode.TRANSIT, parkingCoordonate, finishLocation, alternatives, arrivaleTime, departureTime);
                Date date = Calendar.getInstance().getTime();
                if (GoogleMap.STATUS_OK.equals(trip.status)) {
                    date.setTime(trip.routes.get(0).legs.get(0).departureTime.value);
                    trips.add(googleMapApi.calculTrip(TransportMode.DRIVING, startLocation, parkingCoordonate, alternatives, date, departureTime));
                    trips.add(trip);
                }

            } else if (departureTime != null) {
                Trip trip = googleMapApi.calculTrip(TransportMode.DRIVING, startLocation, parkingCoordonate, alternatives, arrivaleTime, departureTime);
                if (GoogleMap.STATUS_OK.equals(trip.status)) {
                    trips.add(trip);
                    Date date = DateTime.now().withMillis(departureTime.getTime()).plus(trip.routes.get(0).legs.get(0).duration.value).toDate();
                    trips.add(googleMapApi.calculTrip(TransportMode.TRANSIT, parkingCoordonate, finishLocation, alternatives, arrivaleTime, date));
                }
            } else {
                Trip trip = googleMapApi.calculTrip(TransportMode.DRIVING, startLocation, parkingCoordonate, alternatives, arrivaleTime, departureTime);
                if (GoogleMap.STATUS_OK.equals(trip.status)) {
                    trips.add(trip);
                    Date date = DateTime.now().plus(trip.routes.get(0).legs.get(0).duration.value).toDate();
                    trips.add(googleMapApi.calculTrip(TransportMode.TRANSIT, parkingCoordonate, finishLocation, alternatives, arrivaleTime, date));
                }
            }
        }

        return trips;
    }

    @Override
    public List<Trip> getBetterTrips(String startLocation, String finishLocation, Date arrivaleTime, Date departureTime) throws RuntimeException {
        Trip trainTrip = calculTrip(TransportMode.TRANSIT, startLocation, finishLocation, false, arrivaleTime, departureTime);
        Trip carTrip = calculTrip(TransportMode.DRIVING, startLocation, finishLocation, false, arrivaleTime, departureTime);

        List<Park> parks = parkStateService.getAllParks();
        List<List<Trip>> mixTrip = parks.parallelStream()
                .map(park -> calculTripWithParking(TransportMode.DRIVING, startLocation, finishLocation, false, arrivaleTime, departureTime, park.getProperties().getId()))
                .collect(Collectors.toList());

        LocalDateTime now = LocalDateTime.now(); //Optimization

        for(int i = 0; i < mixTrip.size(); i++) {
            Park currentPark = parks.get(i);
            List<Trip> twoTrips = mixTrip.get(i);
            Trip toParking = twoTrips.get(0);

            LocalDateTime arrivalTime = now.plusSeconds((toParking.routes.get(0).legs.get(0).duration.value));
            Park parkForecast = forecastService.getForecast(arrivalTime)
                    .getParks()
                    .stream()
                    .filter(park -> Objects.equals(park.getProperties().getId(), currentPark.getProperties().getId()))
                    .findFirst()
                    .get();

            Integer availablePlaces = parkForecast.getProperties().getFree();

            if(availablePlaces == null || availablePlaces < .9d * parkForecast.getProperties().getTotal())
                mixTrip.remove(twoTrips);
        }


        Trip bestMixTrip = mixTrip.stream()
                .map(trips -> trips.get(0))
                .max(Comparator.comparingInt(trip -> trip.routes.get(0).legs.get(0).duration.value))
                .get();

        List<Trip> bestTrips = new ArrayList<>();
        bestTrips.add(trainTrip);
        bestTrips.add(carTrip);
        bestTrips.add(bestMixTrip);

        return bestTrips;
    }
}
