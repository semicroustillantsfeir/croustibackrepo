package lu.sfeir.codinggame.trip.service;

import lu.sfeir.codinggame.trip.domain.Trip;
import lu.sfeir.codinggame.trip.enu.TransportMode;

import java.util.Date;
import java.util.List;

/**
 * Created by renaudchardin on 12/03/2017.
 */
public interface TripService {
    Trip calculTrip(TransportMode mode, String startLocation, String finishLocation, Boolean  alternatives, Date arrivaleTime, Date departureTime) throws RuntimeException;
    List<Trip> calculTripWithParking(TransportMode mode, String startLocation, String finishLocation, Boolean  alternatives, Date arrivaleTime, Date departureTime, String parkingId) throws RuntimeException;
    List<Trip> getBetterTrips(String startLocation, String finishLocation, Date arrivaleTime, Date departureTime) throws RuntimeException;
}
