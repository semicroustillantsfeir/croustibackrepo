package lu.sfeir.codinggame.trip.enu;

/**
 * Created by renaudchardin on 11/03/2017.
 */
public enum  TransportMode {

    DRIVING ("driving"),
    WALKING ("walking"),
    BICYCLING("bicycling"),
    TRANSIT ("transit");

    private String str;

    // Les constructeurs ne peuvent pas être protected ou public
    private TransportMode(String val)
    {
        str = val;
    }

    public String toString()
    {
        return str;
    }

}
