package lu.sfeir.codinggame.repository;

import lu.sfeir.codinggame.park.domain.ParkState;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.time.LocalDateTime;
import java.util.List;

/**
 * Created by Bastien on 11/03/2017.
 */
public interface ParkStateRepository extends MongoRepository<ParkState, Long> {
    List<ParkState> findByDateBetween(LocalDateTime start, LocalDateTime end);
}