package lu.sfeir.codinggame.constant;

/**
 * Created by renaudchardin on 12/03/2017.
 */
public class URL {
    public static final String TRIP = "/trip";
    public static final String PARKING_TRIP = "/trip/{parkingId}";
    public static final String BEST_TRIP = "/best-trip";
    public static final String PARKS = "/parks";
}
