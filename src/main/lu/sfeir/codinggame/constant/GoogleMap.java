package lu.sfeir.codinggame.constant;

/**
 * Created by renaudchardin on 11/03/2017.
 */
public class GoogleMap {
    public static final String DIRECTION = "https://maps.googleapis.com/maps/api/directions/json?";
    public static final String KEY_PARAM = "&key=AIzaSyDL3ZdonXEGCXPU1rWRObTLub0ks3mgJaM";
    public static final String STATUS_OK = "OK";
    public static final String ORIGIN_PARAM = "origin";
    public static final String DESTINATION_PARAM = "destination";
    public static final String ALTERNATIVES_PARAM = "alternatives";
    public static final String ARRIVAL_TIME_PARAM = "arrival_time";
    public static final String DEPARTURE_TIM_PARAM = "departure_time";
    public static final String MODE_PARAM = "mode";
    public static final String AND = "&";
    public static final String EQUALS = "=";
}
