package lu.sfeir.codinggame.constant;

/**
 * Created by renaudchardin on 11/03/2017.
 */
public class TflUrl {
    public static final String PARK = "https://api.tfl.lu/v1/Occupancy/CarPark";
    public static final String STOP_POINT = "https://api.tfl.lu/v1/StopPoint";
}
