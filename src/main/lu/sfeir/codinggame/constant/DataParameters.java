package lu.sfeir.codinggame.constant;

/**
 * Created by Bastien on 12/03/2017.
 */
public class DataParameters {
    public final static int NUMBER_OF_WEEK_CONSIDERED = 10; //seconds, for the database analysis
    public final static int DUMP_FREQUENCY = 10; //minutes
    public final static int TIME_TOLERANCE = 10;
}
